# README #

This assumes you have Postgis already installed

### Db Install script ###

--Postgresql db creation
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE DATABASE "GeoTesting"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;